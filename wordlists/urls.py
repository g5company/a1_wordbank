from django.conf.urls import url

from . import views

app_name = 'wordlists'
urlpatterns = [
	url(r'^$', views.home_page, name='home'),
	url(r'^detail/(?P<word_text>[a-z,A-Z,0-9]+)', views.detail, name='detail'),
	url(r'^search/', views.search_word, name='search'),
	url(r'^loadcsv/', views.loadcsv, name='loadcsv'),
	url(r'^savecsv/', views.savecsv, name='savecsv'),
	url(r'^downloadcsv/', views.downloadcsv, name='downloadcsv'),
	url(r'^backupxml/', views.backupxml, name='backupxml'),
	url(r'^restorexml/', views.restorexml, name='restorexml'),

]
