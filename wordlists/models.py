import datetime
from django.db import models
from django.utils import timezone

# Create your models here.

class Word(models.Model):
	word_text = models.CharField(max_length=200)
	pub_date = models.DateTimeField('date published') # blank=True, null=True django.utils.timezone.now
	date = models.DateTimeField('date published')
	count = models.IntegerField(default=0)
	def __str__(self):
		return self.word_text
	def was_published_recently(self):
		return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
	def search_date(self):
		return self.date
	def search_count(self):
		return self.count

class Detail(models.Model):
	word = models.ForeignKey(Word, on_delete=models.CASCADE)
	word_type = models.CharField(max_length=10)
	word_meaning = models.CharField(max_length=200)
	word_sentence = models.CharField(max_length=200)
	def __str__(self):
		return self.word_meaning
	def __unicode__(self):
		return self.word_meaning
	
















