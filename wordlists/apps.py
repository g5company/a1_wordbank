from django.apps import AppConfig


class WordlistsConfig(AppConfig):
    name = 'wordlists'
