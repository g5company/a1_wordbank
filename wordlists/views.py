import csv,os
from django.core.urlresolvers import reverse
from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse,HttpResponseRedirect
from django.utils import timezone
from django.core import serializers

from .models import Word,Detail

def home_page(request):
	word = Word.objects.all()
	
	#path = "/home/meaw252/wordbank/csvfile/"

	path = os.path.dirname(os.path.abspath(__file__)) # return this file's path (Ex. ~/wordbank/wordlists)
	splited_path = path.split('/')
	csvpath = '/'.join(splited_path[:-1])+'/csvfile/' # cd to /csvfile (Ex. ~/wordbank/csvfile)
	
	context = {}
	namelist = []
	for file in os.listdir(csvpath):
		filename,ext = os.path.splitext(file)
		if ext == '.csv':
			namelist.append(filename)
	context['backup'] = namelist
	

	if request.method == 'POST':
		word_text = request.POST['word_text']
		word_type = request.POST['word_type']
		word_meaning = request.POST['word_meaning']
		word_sentence = request.POST['word_sentence']
		if word_text and word_meaning and word_sentence: # if string is null; return 'false'
			if Word.objects.filter(word_text=word_text): # if found dulplicate word; add only detail
				w = Word.objects.get(word_text=word_text)
				w.detail_set.create(word_type=word_type,word_meaning=word_meaning,word_sentence=word_sentence)
			else:
				w = Word(word_text=word_text,pub_date=timezone.now(),date=timezone.now(),count=0) 
				w.save()
				w.detail_set.create(word_type=word_type,word_meaning=word_meaning,word_sentence=word_sentence)
		else:
			# no longer use since added 'required' attribute in home.html 's input form #
			context = {}
			if not(word_text):
				context['empty_word'] = 'Please Enter a Word.'
			if not(word_meaning):
				context['empty_meaning'] = 'Please Enter a Meaning.'
			if not(word_sentence):
				context['empty_sentence'] = 'Please Enter a Sentence.'
			context['word'] = word
			return render(request, 'home.html' , context)
			# -- #
	# order_by == sort by min->max
	latest_search_list = Word.objects.order_by('date')[:3]
	most_frequency_search = Word.objects.order_by('count')[:3]
	
	context['latest'] = latest_search_list.reverse()
	context['freq'] = most_frequency_search.reverse()
	context['word'] = word

	return render(request, 'home.html', context)

def detail(request, word_text):
	word = get_object_or_404(Word,word_text=word_text)
	
	return render(request, 'detail.html',{'word':word})

def search_word(request):
	if request.method == 'POST':
		word_text = request.POST['word_text']

	word = get_object_or_404(Word,word_text=word_text)
	word.date = timezone.now()
	word.count = word.count + 1
	word.save()
	
	return render(request, 'detail.html',{'word':word})


def loadcsv(request):
	if request.method == 'POST':
		#filepath = "/home/meaw252/wordbank/csvfile/"+request.POST['timestamp']+'.csv'

		path = os.path.dirname(os.path.abspath(__file__)) # return this file's path (Ex. ~/wordbank/wordlists)
		splited_path = path.split('/')
		csvpath = '/'.join(splited_path[:-1])+'/csvfile/' # cd to /csvfile (Ex. ~/wordbank/csvfile)
		filepath = csvpath+request.POST['timestamp']+'.csv'

		Wordlist = Word.objects.all()

		for w in Wordlist:
			w.delete()
		import codecs
		#with codecs.open(filepath,'r',encoding='utf-8') as csvfile:  # add .encode('utf-8') ?
		with open(filepath,'r') as csvfile:
			reader = csv.DictReader(csvfile)
			for row in reader:
				if row['Word'] != '':
					w = Word(word_text=row['Word'],pub_date=timezone.now(),date=row['Search Date'],count=row['Search Count']) #,date=row['Search Date'],count=row['Search Count']
					w.save()
					w.detail_set.create(word_type=row['Type'],word_meaning=row['Meaning'],word_sentence=row['Sentence'])
				else:
					w.detail_set.create(word_type=row['Type'],word_meaning=row['Meaning'],word_sentence=row['Sentence'])
		return HttpResponseRedirect('/')

def savecsv(request):
	if request.method == 'POST':
		#filepath = "/home/meaw252/wordbank/csvfile/"+str(timezone.now().strftime("%Y-%m-%d %H:%M:%S"))+'.csv'

		path = os.path.dirname(os.path.abspath(__file__)) # return this file's path (Ex. ~/wordbank/wordlists)
		splited_path = path.split('/')
		csvpath = '/'.join(splited_path[:-1])+'/csvfile/' # cd to /csvfile (Ex. ~/wordbank/csvfile)
		filepath = csvpath+str(timezone.now().strftime("%Y-%m-%d_%H:%M:%S"))+'.csv'

		with open(filepath,'w') as csvfile:
			writer = csv.writer(csvfile)
			writer.writerow(['Word','Type','Meaning','Sentence','Search Date','Search Count'])
			for word in Word.objects.all():
				index = 0
				for detail in word.detail_set.all():
					if index==0:
						writer.writerow([word.word_text,detail.word_type,detail.word_meaning.encode('utf-8'),detail.word_sentence,word.date,word.count])
					else:
						writer.writerow(['',detail.word_type,detail.word_meaning.encode('utf-8'),detail.word_sentence],word.date,word.count)
					index=index+1
	return HttpResponseRedirect('/')

def downloadcsv(request):
	response = HttpResponse (content_type = 'text/csv')
	response['Content-Disposition'] = 'attachment; filename='+str(timezone.now().strftime("%Y-%m-%d_%H:%M:%S"))+'.csv'
	
	writer = csv.writer(response)
	writer.writerow(['Word','Type','Meaning','Sentence'])
	for word in Word.objects.all():
		index = 0
		for detail in word.detail_set.all():
			if index==0:
				writer.writerow([word.word_text,detail.word_type,detail.word_meaning.encode('utf-8'),detail.word_sentence])
			else:
				writer.writerow(['',detail.word_type,detail.word_meaning.encode('utf-8'),detail.word_sentence])
			index=index+1
	return response

def backupxml(request):
	if request.method == 'POST':
		with open("file.xml","w")as out:
			XMLSerializer = serializers.get_serializer("xml")
			xml_serializer = XMLSerializer()

			all_objects = list(Word.objects.all()) + list(Detail.objects.all())
			xml_serializer.serialize(all_objects,stream=out)
			data = xml_serializer.serialize(all_objects)
			for deserialized_object in serializers.deserialize("xml",data):
				deserialized_object.save()
			

	return HttpResponseRedirect('/')

def restorexml(request):
	if request.method == 'POST':
		'''
		with open("file.xml","r")as data:
			for deserialized_object in serializers.deserialize("xml",data):
				if object_should_be_saved(deserialized_object):
					deserialized_object.save()
		'''
		#import xml.etree.ElementTree as ET
		#tree = ET.parse('file.xml')
		#root = tree.getroot()

		#for child in root:
			#print child.tag, child.attrib
		'''
		for obj in root.iter('object'):
			if(obj.get('model')=='wordlists.word'):
				word_pk = obj.get('pk')
				for field in root.iter('field'):
					#print field.attrib
					if(field.get('name')=='word_text'):
						word_text = field.text
					if(field.get('name')=='pub_date'):
						pub_date = field.text
					#print field.get('name')
					print '*',field.text
				w = Word(word_text=word_text,pub_date=pub_date)
				w.save()

				for obj in root.iter('object'):
					if(obj.get('model')=='wordlists.detail'):
						for field in root.iter('field'):
							if(field.get('name')=='word'):
								rel_pk = field.text
							if(field.get('name')=='word_type'):
								word_type = field.text
							if(field.get('name')=='word_meaning'):
								word_meaning = field.text
							if(field.get('name')=='word_sentence'):
								word_sentence = field.text
						if(rel_pk == word_pk):
							w.detail_set.create(word_type=word_type,word_meaning=word_meaning,word_sentence=word_sentence)
		'''
		#for obj in root.findall('object'):
			#print obj.attrib
			#name = obj.find('field').text
			#print name
			
		

	return HttpResponseRedirect('/')


'''
class Search(models.Model):
	#word = models.ForeignKey(Word, on_delete=models.CASCADE)
	search_text = models.CharField(max_length=200 ,null=True)
	search_date = models.DateTimeField('date published')
	search_count = models.IntegerField(default=0)
	def __str__(self):
		return self.search_text
'''












