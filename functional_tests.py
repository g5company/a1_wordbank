from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest

class NewVisitorTest(unittest.TestCase):

	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)

	def tearDown(self):
		self.browser.quit()

	def test_can_start_wordbank_and_retrieve_it_later(self):
		# User come to our Word Bank website
		self.browser.get('http://localhost:8000')

		# She look at the page title and header mention Word Bank
		self.assertIn('Word Bank',self.browser.title)
		header_text = self.browser.find_element_by_tag_name('h1').text
		self.assertIn('Word Bank',header_text)

		# She is invited to enter a word into text box
		inputbox = self.browser.find_element_by_id('id_new_word')
		self.assertEqual(inputbox.get_attribute('placeholder'),'Enter a word')
		meaningbox = self.browser.find_element_by_id('meaning')
		self.assertEqual(inputbox.get_attribute('placeholder'),'Enter a meaning')
		sentencebox = self.browser.find_element_by_id('sentence')
		self.assertEqual(inputbox.get_attribute('placeholder'),'Enter a sentence')
		
		
		# She types "Cat" into a text box
		inputbox.send_keys('Cat')
		meaningbox.send_keys('An animal with 4 legs')
		sentencebox.send_keys('Malee has a baby cat')

		# When she hits enter, the page updates, and now the word display
		inputbox.send_keys(Keys.ENTER)

		table = self.browser.find_element_by_id('id_list_word')
		rows = table.find_elements_by_tag_name('tr')
		self.assertIn('Cat',[row.text for row in rows])
		self.assertIn('An animal with 4 legs',[row.text for row in rows])
		self.assertIn('Malee has a baby cat',[row.text for row in rows])

		# There is still a text box to add another item. She enters "Homework".
		inputbox = self.browser.find_element_by_id('id_new_word')
		inputbox.send_keys('Homework')
		inputbox.send_keys(Keys.ENTER)

		# The page updates again, and now shows both words in list
		table = self.browser.find_element_by_id('id_list_word')
		rows = table.find_elements_by_tag_name('tr')
		self.assertIn('Cat',[row.text for row in rows])
		self.assertIn('Homework',[row.text for row in rows])

		# 
		self.fail('Finish the test!')

if __name__ == '__main__':
	unittest.main(warnings='ignore')
